// Contains all the endpoints for out application

// We need to user express' Router() function to achieve this.

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for out application
const router = express.Router();

const taskController = require("../controllers/taskController");

/*
	Syntax :
	localhost:3002/tasks/getinfo

*/


// [SECTION] Routes
router.get("/getinfo/:id", (req, res) => {
taskController.getAllTasks(req.params.id).then(
	resultFromController => res.send(resultFromController));

})






// Route to create a new task

router.post("/create",(req, res) => {

// If information will be coming from client side commonly from forms, the data can be accessed from the request "body" property.
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// The colon (:) is an idenetifier that helps create a dynamic route which allows us to supply information in the URL
//  The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where tou can put any value, it then creates a link between "id" parameter in the URL and the value provide in the URL

/*
	Ex.
	localhost:3001/tasks/delete/1234
	the 1234 is assigned to the "id" parameter in the URL

*/
router.delete("/delete/:id", (req, res) => {

	// URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	// If information will be coming from the URL, the data can be accessed from the request "params" property

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController)) ;
})

// Route to update a task
router.put("/update/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})




// Use "module.exports" to exports the router object to use in the "app.js"

module.exports = router;